/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useEffect, useRef } from 'react';
import {
  StyleSheet,
  View, Alert
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { check, PERMISSIONS, RESULTS, request, openSettings } from 'react-native-permissions';
import MapView, { PROVIDER_GOOGLE, Callout, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps

const callgeolocation = (callback) => {
  Geolocation.getCurrentPosition(
    (position) => {
      console.log(position);
      callback(position)
    },
    (error) => {
      // See error code charts below.
      console.log(error.code, error.message);
    },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
  )
};
export default () => {
  const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
  });

  const MapViewRef = useRef()

  useEffect(() => {
    check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      .then(result => {
        if (result === RESULTS.GRANTED) {
          callgeolocation(position => {
            MapViewRef.current.animatecamera({
              center: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              }
            })
          })

        }
        if (result === RESULTS.DENIED) {

          request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
            if (result === RESULTS.GRANTED) {
              callgeolocation()
            }
            else if (result === RESULTS.DENIED) {
              alert('5555')
            }
            else if (result === RESULTS.BLOCKED) {
              alert('bye')
            }
          })
          return
        }
        if (result === RESULTS.BLOCKED) {
          Alert.alert(
            'Alert Title',
            'คุณต้องเลือกว่าจะทำยังไงกับแอพ',
            [
              {
                text: 'Open settings', onPress: () => openSettings()
              },
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
          );
        }
      }
      )


  }, [])

  return (
    <View style={styles.container}>
      <MapView
        ref={MapViewRef}
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
      >
        {/* <Marker ...}>
        <MyCustomMarkerView {...marker} />
        <Callout>
          <MyCustomCalloutView {...marker} />
        </Callout>
      </Marker> */}
      </MapView>
    </View>
  )
}
